# Vue example - Grid Component

## Used technologies

* VueJS 2
* Vuetify 0.0.17
* VueResource

## Component description

This component is good for displaying tabular remote data with simple
and extendability. It is used in some of our projects in backend.