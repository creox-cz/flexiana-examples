<?php
namespace App\Import;
use App\Import\Models\Enums\LogStatus;
class Watch extends Base
{
    protected $countries = [];
    protected $types = [];
    protected $caseMaterials = [];
    protected $braceletMaterials = [];
    public function saveItem(\App\Import\DataTransferObject\Watch $watch): bool
    {
        /**
         * @var \App\Import\DataTransferObject\Watch $watch
         */
        $watch = $this->convertToUtf8($watch);
        $logStatus = LogStatus::STATUS_SUCCESSFULLY;
        $logMessage = "";
        $isItemSaved = false;
        $makeAndModelAndSubModel = $this->getEmptyMakeAndModelAndSubModel();
        try {
            if (!is_null($watch->makeAndModel)) {
                $makeAndModelAndSubModel = $this->createMakeAndModelAndSubModel(
                    $watch->makeAndModel,
                    $watch->productType
                );
            }
            $watchType = $this->getType($watch->watchType);
            $caseMaterial = $this->getCaseMaterial($watch->watchCaseMaterial);
            $braceletMaterial = $this->getBraceletMaterial($watch->watchBraceletMaterial);
        } catch (\Exception $exception) {
            $logStatus = LogStatus::STATUS_ITEM_SAVE_PROPERTY_ERROR;
            $logMessage .= "\nException message:\n";
            $logMessage .= $exception->getMessage();
            $logMessage .= "\n";
        }
        $watchItem = new \App\Models\Watch();
        $watchItem->productType = $watch->productType;
        $watchItem->watchMakeId = $makeAndModelAndSubModel["makeId"];
        $watchItem->watchModelId = $makeAndModelAndSubModel["modelId"];
        $watchItem->watchSubModelId = $makeAndModelAndSubModel["subModelId"];
        $watchItem->watchTypeId = is_null($watchType) ? null : $watchType->id;
        $watchItem->userId = $watch->userId;
        $watchItem->companyId = $watch->companyId;
        $watchItem->watchCaseMaterialId = is_null($caseMaterial) ? null : $caseMaterial->id;
        $watchItem->watchBraceletMaterialId = is_null($braceletMaterial) ? null : $braceletMaterial->id;
        $watchItem->externalId = $watch->externalId;
        $watchItem->modelVariant = $watch->modelVariant;
        //$watchItem->condition = $this->translateContions($this);
        $watchItem->movement = $this->translateMovement($watch->movement);
        $watchItem->description = $watch->description;
        $watchItem->manufacturedYear = $watch->manufacturedYear;
        $watchItem->diameter = $watch->diameter;
        $watchItem->referenceNumber = $watch->referenceNumber;
        $watchItem->stayAnonymous = $watch->stayAnonymous;
        $watchItem->country = strtoupper($watch->country);
        $watchItem->gender = $this->getGenreByType($watch->watchType);
        $watchItem->createdAt = (new \DateTime())->setTimestamp($watch->created)->format('Y-m-d H:i:s');
        $watchItem->updatedAt = is_null($watch->updated) ? null :
            (new \DateTime())->setTimestamp($watch->updated)->format('Y-m-d H:i:s');
        if ($watch->listing && $watch->listing->deleted) {
            $watchItem->deletedAt = (new \DateTime())->setTimestamp($watch->updated)->format('Y-m-d H:i:s');
        }
        $watchItem->dialColor = null;
        $watchItem->braceletColor = null;
        $watchItem->editorialTitleDE = $watch->editorialTitleDE;
        $watchItem->editorialTitleEN = $watch->editorialTitleEN;
        try {
            $isItemSaved = $watchItem->save();
        } catch (\Exception $exception) {
            $logStatus = LogStatus::STATUS_ITEM_NOT_SAVED;
            $isItemSaved = false;
            $logMessage .= "\nException message:\n";
            $logMessage .= $exception->getMessage();
            $logMessage . "\n";
        }
        if ($isItemSaved) {
            try {
                echo "Item Id: " . $watchItem->id;
                $this->saveFiles(
                    $watchItem->id,
                    $watch->nid,
                    $watch->productType,
                    \App\Import\Base::IMPORT_FILE_TYPE_IMAGE,
                    $watch->images
                );
                $this->saveFiles(
                    $watchItem->id,
                    $watch->nid,
                    $watch->productType,
                    \App\Import\Base::IMPORT_FILE_TYPE_VIDEO,
                    $watch->videos
                );
                $listedProduct = $this->createListedProduct($watchItem->id, $watch);
                $this->createPrice($watch, $listedProduct);
                $this->createProductInquiries($listedProduct, $watch->productInquiries);
                //mapping old databese to new database
                $this->saveMappingItem($watchItem->id, $watch->nid, $watch->productType);
            } catch (\Exception $e) {
                $logStatus = LogStatus::STATUS_ITEM_SAVE_PROPERTY_ERROR;
                $logMessage .= "\nException message:\n";
                $logMessage .= $e->getMessage();
                $logMessage . "\n";
                $logMessage .= "\n Print data transfer object:\n";
                $logMessage .= var_export($watch, true);
                $logMessage . "\n";
                $logMessage .= "\n Print model object:\n";
                $logMessage .= var_export($watchItem->toArray(), true);
            }
        }
        try {
            $this->log($watch->nid, $watchItem->id, $watch->productType, $logStatus, $logMessage);
        } catch (\Exception $exception) {
            return $isItemSaved;
        }
        return $isItemSaved;
    }
    public function getType($type)
    {
        if (is_null($type)) {
            return null;
        }
        if (array_key_exists($type, $this->types)) {
            return $this->types[$type];
        }
        $typeDb = \App\Models\Watch\Type::findFirst([
            "conditions" => "code = :code:",
            "bind" => ["code" => $type],
        ]);
        if ($typeDb) {
            $this->types[$type] = $typeDb;
        } else {
            $nType = new \App\Models\Watch\Type();
            $nType->code = $type;
            $nType->save();
            $this->types[$type] = $nType;
        }
        return $this->types[$type];
    }
    public function getCaseMaterial($caseMaterial)
    {
        if (is_null($caseMaterial)) {
            return null;
        }
        if (array_key_exists($caseMaterial, $this->caseMaterials)) {
            return $this->caseMaterials[$caseMaterial];
        }
        $cMaterialDb = \App\Models\Watch\CaseMaterial::findFirst([
            "conditions" => "code = :code:",
            "bind" => ["code" => $caseMaterial],
        ]);
        if ($cMaterialDb) {
            $this->caseMaterials[$caseMaterial] = $cMaterialDb;
        } else {
            $cMaterial = new \App\Models\Watch\CaseMaterial();
            $cMaterial->code = $caseMaterial;
            $cMaterial->save();
            $this->caseMaterials[$caseMaterial] = $cMaterial;
        }
        return $this->caseMaterials[$caseMaterial];
    }
    public function getBraceletMaterial($braceletMaterial)
    {
        if (is_null($braceletMaterial)) {
            return null;
        }
        if (array_key_exists($braceletMaterial, $this->braceletMaterials)) {
            return $this->braceletMaterials[$braceletMaterial];
        }
        $bMaterialDb = \App\Models\Watch\BraceletMaterial::findFirst([
            "conditions" => "code = :code:",
            "bind" => ["code" => $braceletMaterial],
        ]);
        if ($bMaterialDb) {
            $this->braceletMaterials[$braceletMaterial] = $bMaterialDb;
        } else {
            $cMaterial = new \App\Models\Watch\BraceletMaterial();
            $cMaterial->code = $braceletMaterial;
            $cMaterial->save();
            $this->braceletMaterials[$braceletMaterial] = $cMaterial;
        }
        return $this->braceletMaterials[$braceletMaterial];
    }
    public function translateMovement($movement)
    {
        switch ($movement) {
            case "Automatic":
                return \App\Models\Enums\Watch\Movement::MOVEMENT_AUTOMATIC;
            case "Handwinding":
                return \App\Models\Enums\Watch\Movement::MOVEMENT_HANDWINDING;
            case "Quartz":
            case "Unknown":
            default:
                return \App\Models\Enums\Watch\Movement::MOVEMENT_OTHER;
        }
    }
    public function getGenreByType($type)
    {
        if (is_null($type)) {
            return null;
        }
        switch ($type) {
            case "Men/Unisex":
                return \App\Models\Enums\Watch\Gender::GENDER_UNISEX;
            case "Women":
                return \App\Models\Enums\Watch\Gender::GENDER_LADIES;
        }
    }
}