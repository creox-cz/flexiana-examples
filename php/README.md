# PHP example

## Import.php (Zdeněk Jirků <zj@creox.cz>)

This file is example from large data import based on Phalcon Framework

## Image.php (Václav Kozelka <vk@creox.cz>)

This file is for image manipulation used in many of our projects. Also using
Phalcon Framework and PHP library (GD or ImageMagick)

## Entity.php (Václav Kozelka <vk@creox.cz>)

This is my implementation of Entity-Attribute-Value designed for use with
Phalcon Framework.

It is usefull for very dynamic database objects so every record is saved in 
entity table (for auto_increment value) and all entities can have unlimited
count of fields (limit is only MySQL table column limit).

## Entity/Type.php (Václav Kozelka <vk@creox.cz>)

There is list of entity types (for example: product, order, catalog_item, car,
etc..). There is not any limit in entity type count

## Entity/Field.php (Václav Kozelka <vk@creox.cz>)

There is model for defined fields which can be assigned to each entity type
Field can also have options for different field frontends (text, textarea, 
select, checkbox), @see _Entity/Field/Option.php_

## Entity/*Exception.php (Václav Kozelka <vk@creox.cz>)

There is exception classes if something goes wrong with entity (for now: not 
found and not saved)

## Entity/Data/*.php (Václav Kozelka <vk@creox.cz>)

There is models for every backend type of enitty fields. I took same names like
MySQL data types. Form my point this list is complete for approx 90% websites