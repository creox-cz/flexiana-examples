<?php
namespace Cms\Core\Service;

use Phalcon\Http\Request\File;
use Phalcon\Image\Adapter;

class Image {

    public function get($width, $height = null) {
        return "https://picsum.photos/".$width.($height?"/".$height:"")."?random";
    }

    public function thumbnail($id, $params = [])
    {
        $image = \Cms\Core\Model\Image::findFirst(intval($id));
        if (!$image) {
            return $this->get(0,0);
        }

        $imageFullPath = DIR_ROOT.DS."www".DS.trim($image->path,DS).DS.$image->filename;
        try {
            $modes = $params["modes"];
            unset($params["modes"]);
            $imageObject = $this->__processImage(new \Phalcon\Image\Adapter\Gd($imageFullPath), $modes, $params);
            $this->__checkDirectory(DIR_ROOT.DS."www".DS."thumbnails".DS.trim($image->path,DS).DS.md5(serialize($params)));
            $imageObject->save(DIR_ROOT.DS."www".DS."thumbnails".DS.trim($image->path,DS).DS.md5(serialize($params)).DS.$image->filename);
            return "/thumbnails/".trim($image->path,"/")."/".md5(serialize($params))."/".$image->filename;
        } catch (\Exception $e) {
            echo "IMAGE Exception: ".$e->getMessage();exit;
        }

        return $imageFullPath;
    }

    public function downloadFromUrl($url, $target) {
        $filepath = $this->downloadFileFromUrl($url);
        $filename = basename($filepath);
        $targetDir = DIR_ROOT.DS."www".DS.$target;
        $this->__checkDirectory($targetDir);

        $filename = preg_replace("/(![a-zA-Z0-9])/","-", $filename);
        $imageObject = null;

        if (file_exists($targetDir.DS.$filename)) {
            if (filesize($filepath) != filesize($targetDir.DS.$filename)) {
                if (copy($filepath, $targetDir.DS.$filename)) {
                    list ($width, $height) = getimagesize($targetDir . DS . $filename);
                    unlink($filepath);
                    $imageObject = (\Cms\Core\Model\Image::findFirstByFilename($filename));
                    if (!$imageObject) {
                        $imageObject = new \Cms\Core\Model\Image();
                    }
                    $imageObject->save([
                        "filename" => $filename,
                        "path" => str_replace(DIR_ROOT . DS . "www", "", $targetDir),
                        "size" => filesize($targetDir . DS . $filename),
                        "width" => $width,
                        "height" => $height
                    ]);
                    return $imageObject;
                } else {
                    throw new \Exception("Cannot copy file");
                }
            } else {
                unlink($filepath);
                return (\Cms\Core\Model\Image::findFirstByFilename($filename));
            }
        } else {
            if (copy($filepath, $targetDir.DS.$filename)) {
                list ($width, $height) = getimagesize($targetDir.DS.$filename);
                unlink($filepath);
                $imageObject = new \Cms\Core\Model\Image();
                $imageObject->save([
                    "filename" => $filename,
                    "path" => str_replace(DIR_ROOT . DS . "www", "", $targetDir),
                    "size" => filesize($targetDir . DS . $filename),
                    "width" => $width,
                    "height" => $height
                ]);
                return $imageObject;
            } else {
                throw new \Exception("Cannot copy file");
            }
        }
    }

    public function upload(File $file, $target) {
        $filename = $file->getName();
        $targetDir = dirname(DIR_ROOT.DS."www".DS.$target);
        $this->__checkDirectory($targetDir);

        $filename = preg_replace("/(![a-zA-Z0-9])/","-", $filename);

        if (file_exists($targetDir.DS.$filename)) {
            if (filesize($file->getTempName()) != filesize($targetDir.DS.$filename)) {
                if ($file->moveTo($targetDir.DS.$filename)) {
                    list ($width, $height) = getimagesize($targetDir . DS . $filename);
                    (\Cms\Core\Model\Image::findFirstByFilename($filename))->save([
                        "filename" => $filename,
                        "path" => str_replace(DIR_ROOT . DS . "www", "", $targetDir),
                        "size" => filesize($targetDir . DS . $filename),
                        "width" => $width,
                        "height" => $height
                    ]);
                }
            }
        } else {
            if ($file->moveTo($targetDir.DS.$filename)) {
                list ($width, $height) = getimagesize($targetDir.DS.$filename);
                (new \Cms\Core\Model\Image())->save([
                    "filename" => $filename,
                    "path" => str_replace(DIR_ROOT.DS."www","",$targetDir),
                    "size" => filesize($targetDir.DS.$filename),
                    "width" => $width,
                    "height" => $height
                ]);
            }
        }
    }

    public function downloadFileFromUrl($url) {
        $targetFile = DIR_ROOT.DS."var".DS."download".DS.basename($url);
        $targetDir = dirname($targetFile);
        $this->__checkDirectory($targetDir);

        $curlOptions = [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FILE => fopen($targetFile,"w+"),
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_URL => $url,
            CURLOPT_FAILONERROR => true
        ];
        $curl = curl_init();
        \curl_setopt_array($curl, $curlOptions);
        $result = curl_exec($curl);

        if ($result === false) {
            throw new \Exception(curl_error($curl));
        } else {
            return $targetFile;
        }
    }

    private function __checkDirectory($dir) {
        if (!file_exists($dir) || !is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        if (!is_writeable($dir)) {
            chmod($dir, 0777);
        }
        if (!is_writeable($dir)) {
            throw new \Exception("Directory ".$dir." is not writable");
        }
        return true;
    }

    private function __processImage(Adapter $image, array $modes = [], array $params) : Adapter {
        $availableModes = ["resize","crop","rotate","flip","sharp","blur","pixel"];
        foreach ($modes as $mode) {
            if (in_array($mode, $availableModes)) {
                $method = "__process".ucfirst($mode);
                $image = $this->{$method}($image, $params);
            }
        }
        return $image;
    }

    private function __processResize(Adapter $image, array $params): Adapter {
        switch ($params["resizeMode"]) {
            case "width":
                return $image->resize($params["resizeWidth"],$params["resizeHeight"], \Phalcon\Image::WIDTH);
                break;
            case "height":
                return $image->resize($params["resizeWidth"],$params["resizeHeight"], \Phalcon\Image::HEIGHT);
                break;
            case "none":
                return $image->resize($params["resizeWidth"],$params["resizeHeight"], \Phalcon\Image::NONE);
                break;
            case "tensile":
                return $image->resize($params["resizeWidth"],$params["resizeHeight"], \Phalcon\Image::TENSILE);
                break;
            case "inverse":
                return $image->resize($params["resizeWidth"],$params["resizeHeight"], \Phalcon\Image::INVERSE);
                break;
            case "precise":
                return $image->resize($params["resizeWidth"],$params["resizeHeight"], \Phalcon\Image::PRECISE);
                break;
            case "auto":
            default:
                return $image->resize($params["resizeWidth"],$params["resizeHeight"], \Phalcon\Image::AUTO);
                break;
        }
    }

    private function __processCrop(Adapter $image, array $params) : Adapter {
        $offsetX = intval(($image->getWidth() / 2) - ($params["cropWidth"] / 2));
        $offsetY = intval(($image->getHeight() / 2) - ($params["cropHeight"] / 2));
        switch ($params["cropMode"]) {
            case 'topLeft':
                $offsetX = 0;
                $offsetY = 0;
                break;
            case 'topCenter':
                $offsetX = intval(($image->getWidth() / 2) - ($params["cropWidth"] / 2));
                $offsetY = 0;
                break;
            case 'topRight':
                $offsetX = $image->getWidth() - $params["cropWidth"];
                $offsetY = 0;
                break;
            case 'middleLeft':
                $offsetX = 0;
                $offsetY = intval(($image->getHeight() / 2) - ($params["cropHeight"] / 2));;
                break;
            case 'middleCenter':
                $offsetX = intval(($image->getWidth() / 2) - ($params["cropWidth"] / 2));
                $offsetY = intval(($image->getHeight() / 2) - ($params["cropHeight"] / 2));
                break;
            case 'middleRight':
                $offsetX = $image->getWidth() - $params["cropWidth"];
                $offsetY = intval(($image->getHeight() / 2) - ($params["cropHeight"] / 2));
                break;
            case 'bottomLeft':
                $offsetX = 0;
                $offsetY = $image->getHeight() - $params["cropHeight"];
                break;
            case 'bottomCenter':
                $offsetX = intval(($image->getWidth() / 2) - ($params["cropWidth"] / 2));
                $offsetY = $image->getHeight() - $params["cropHeight"];
                break;
            case 'bottomRight':
                $offsetX = $image->getWidth() - $params["cropWidth"];
                $offsetY = $image->getHeight() - $params["cropHeight"];
                break;
        }

        return $image->crop($params["cropWidth"],$params["cropHeight"],$offsetX, $offsetY);
    }

    private function __processRotate(Adapter $image, array $params) : Adapter {
        return $image->rotate($params["rotateAngle"]);
    }

    private function __processFlip(Adapter $image, array $params) : Adapter {
        switch ($params["flipMode"]) {
            case "horizontal":
                return $image->flip(\Phalcon\Image::HORIZONTAL);
                break;
            case "vertical":
                return $image->flip(\Phalcon\Image::VERTICAL);
                break;
        }
    }

    private function __processSharp(Adapter $image, array $params) : Adapter {
        return $image->sharpen($params["sharpPercent"]);
    }

    private function __processBlur(Adapter $image, array $params) : Adapter {
        return $image->blur($params["blurRadius"]);
    }

    private function __processPixel(Adapter $image, array $params) : Adapter {
        return $image->pixelate($params["pixelAmount"]);
    }

}