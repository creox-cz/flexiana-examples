<?php
namespace Cms\Core\Model\Entity;

use Cms\Core\Model\BaseModel;

class Type extends BaseModel {

    public function getSource()
    {
        return "entity_type";
    }

    public function initialize()
    {
        parent::initialize();

        $this->hasMany(
            "id_entity_type",
            "\\Cms\\Core\\Model\\Entity",
            "id_entity_type",
            [
                "alias" => "entity"
            ]
        );

        $this->hasMany(
            "id_entity_type",
            "\\Cms\\Core\\Model\\Entity\\Type\\Field",
            "id_entity_type",
            [
                "alias" => "entity_type_field"
            ]
        );

        $this->hasManyToMany(
            "id_entity_type",
            "\\Cms\\Core\\Model\\Entity\\Type\\Field",
            "id_entity_type",
            "id_entity_field",
            "\\Cms\\Core\\Model\\Entity\\Field",
            "id_entity_field",
            [
                "alias" => "entity_field"
            ]
        );
    }

    /**
     * Return all \Cms\Core\Model\Entity
     *
     * @param null $params
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public function getEntity($params = null)
    {
        return $this->getRelated("entity", $params);
    }

    /**
     * Return all \Cms\Core\Model\Entity\Type\Field
     *
     * @param null $params
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public function getEntityTypeField($params = null)
    {
        return $this->getRelated("entity_type_field", $params);
    }

    /**
     * Return all \Cms\Core\Model\Entity\Field
     *
     * @param null $params
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public function getEntityField($params = null)
    {
        return $this->getRelated("entity_field", $params);
    }

}