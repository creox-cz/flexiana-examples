<?php
namespace Cms\Core\Model\Entity\Type;

use Cms\Core\Model\BaseModel;

class Field extends BaseModel {

    public function getSource()
    {
        return "entity_type_field";
    }

    public function initialize()
    {
        parent::initialize();
    }

}