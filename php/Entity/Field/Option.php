<?php
namespace Cms\Core\Model\Entity\Field;

use Cms\Core\Model\BaseModel;

class Option extends BaseModel {

    public function getSource()
    {
        return "entity_field_option";
    }

    public function initialize()
    {
        parent::initialize();
    }

}