<?php
namespace Cms\Core\Model\Entity\Field\Option;

use Cms\Core\Model\BaseModel;

class Label extends BaseModel {

    public function getSource()
    {
        return "entity_field_option_label";
    }

    public function initialize()
    {
        parent::initialize();
    }

}