<?php
namespace Cms\Core\Model\Entity\Data;

use Cms\Core\Model\BaseModel;

class Date extends BaseModel {

    public function getSource()
    {
        return "entity_data_date";
    }

    public function initialize()
    {
        parent::initialize();
    }

}