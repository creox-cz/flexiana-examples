<?php
namespace Cms\Core\Model\Entity\Data;

use Cms\Core\Model\BaseModel;

class Varchar extends BaseModel {

    public function getSource()
    {
        return "entity_data_varchar";
    }

    public function initialize()
    {
        parent::initialize();
    }

}