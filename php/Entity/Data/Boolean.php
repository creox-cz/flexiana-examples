<?php
namespace Cms\Core\Model\Entity\Data;

use Cms\Core\Model\BaseModel;

class Boolean extends BaseModel {

    public function getSource()
    {
        return "entity_data_boolean";
    }

    public function initialize()
    {
        parent::initialize();
    }

}