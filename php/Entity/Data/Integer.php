<?php
namespace Cms\Core\Model\Entity\Data;

use Cms\Core\Model\BaseModel;

class Integer extends BaseModel {

    public function getSource()
    {
        return "entity_data_integer";
    }

    public function initialize()
    {
        parent::initialize();
    }

}