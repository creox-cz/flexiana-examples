<?php
namespace Cms\Core\Model\Entity\Data;

use Cms\Core\Model\BaseModel;

class Text extends BaseModel {

    public function getSource()
    {
        return "entity_data_text";
    }

    public function initialize()
    {
        parent::initialize();
    }

}