<?php
namespace Cms\Core\Model\Entity\Data;

use Cms\Core\Model\BaseModel;

class Datetime extends BaseModel {

    public function getSource()
    {
        return "entity_data_datetime";
    }

    public function initialize()
    {
        parent::initialize();
    }

}