<?php
namespace Cms\Core\Model\Entity\Data;

use Cms\Core\Model\BaseModel;

class Point extends BaseModel {

    public function getSource()
    {
        return "entity_data_point";
    }

    public function initialize()
    {
        parent::initialize();
    }

}