<?php
namespace Cms\Core\Model\Entity\Data;

use Cms\Core\Model\BaseModel;

class Decimal extends BaseModel {

    public function getSource()
    {
        return "entity_data_decimal";
    }

    public function initialize()
    {
        parent::initialize();
    }

}