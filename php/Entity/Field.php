<?php
namespace Cms\Core\Model\Entity;

use Cms\Core\Model\BaseModel;

class Field extends BaseModel {

    public function getSource()
    {
        return "entity_field";
    }

    public function initialize()
    {
        parent::initialize();
    }

}