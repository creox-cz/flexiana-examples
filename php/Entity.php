<?php
namespace Cms\Core\Model;

use Cms\Core\Model\Entity\EntityNotFoundException;
use Cms\Core\Model\Entity\EntityNotStoredException;
use Cms\Core\Model\Entity\Type;
use Phalcon\Debug\Dump;
use Phalcon\Mvc\Model\Query\Builder;

class Entity extends BaseModel {

    public function getSource()
    {
        return "entity";
    }

    public static function getType()
    {
        return "default";
    }

    public static function getIndexedModel()
    {
        return "\\Cms\\Core\\Model\\Entity";
    }

    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            "id_entity_type",
            "\\Cms\\Core\\Model\\Entity\\Type",
            "id_entity_type",
            [
                "alias" => "entity_type"
            ]
        );

        $this->hasMany(
            "id_entity",
            "\\Cms\\Core\\Model\\Entity\\Data\\Boolean",
            "id_entity",
            [
                "alias" => "entity_data_boolean"
            ]
        );

        $this->hasMany(
            "id_entity",
            "\\Cms\\Core\\Model\\Entity\\Data\\Decimal",
            "id_entity",
            [
                "alias" => "entity_data_decimal"
            ]
        );

        $this->hasMany(
            "id_entity",
            "\\Cms\\Core\\Model\\Entity\\Data\\Integer",
            "id_entity",
            [
                "alias" => "entity_data_integer"
            ]
        );

        $this->hasMany(
            "id_entity",
            "\\Cms\\Core\\Model\\Entity\\Data\\Text",
            "id_entity",
            [
                "alias" => "entity_data_text"
            ]
        );

        $this->hasMany(
            "id_entity",
            "\\Cms\\Core\\Model\\Entity\\Data\\Varchar",
            "id_entity",
            [
                "alias" => "entity_data_varchar"
            ]
        );

        $this->hasMany(
            "id_entity",
            "\\Cms\\Core\\Model\\Entity\\Data\\Point",
            "id_entity",
            [
                "alias" => "entity_data_point"
            ]
        );

        $this->hasMany(
            "id_entity",
            "\\Cms\\Core\\Model\\Entity\\Data\\Date",
            "id_entity",
            [
                "alias" => "entity_data_date"
            ]
        );

        $this->hasMany(
            "id_entity",
            "\\Cms\\Core\\Model\\Entity\\Data\\Datetime",
            "id_entity",
            [
                "alias" => "entity_data_datetime"
            ]
        );
    }

    /**
     * Return all \Cms\Core\Model\Entity\Type
     *
     * @param null $params
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public function getEntityType($params = null) {
        return $this->getRelated("entity_type", $params);
    }

    public static function builder($entityType = null, $forceNotIndexed = false, $morphPoints = true)
    {
        if (!\Phalcon\Di::getDefault()->get("config")->application->db->useIndexedTables || true === $forceNotIndexed) {
            // Load all atributes assigned for type
            $type = Type::findFirstByCode($entityType ? $entityType : static::getType());
            if (!$type) {
                throw new Type\TypeNotFoundException();
            }
            $fields = $type->getEntityField();

            $joins = [];
            $columns = [
                "id_entity" => "e.id_entity",
                "id_entity_type" => "e.id_entity_type",
            ];
            foreach ($fields as $field) {
                $joins[] = [
                    "conditions" => "e.id_entity = ".$field->name.".id_entity AND ".$field->name.".id_entity_field = ".$field->id_entity_field,
                    "model" => "\\Cms\\Core\\Model\\Entity\\Data\\".ucfirst($field->backend),
                    "alias" => $field->name
                ];
                if ($field->backend == "point" && true === $morphPoints) {
                    // Point have specific values
                    $columns[$field->name."_lat"] = "ST_X(".$field->name.".value)";
                    $columns[$field->name."_lon"] = "ST_Y(".$field->name.".value)";
                } else {
                    $columns[$field->name] = $field->name.".value";
                }
            }
            $columns["created_at"] = "e.created_at";
            $columns["updated_at"] = "e.updated_at";
            $columns["deleted_at"] = "e.deleted_at";

            // Prepare builder with all attributes
            $builder = new Builder();
            $builder->from(["e" => "\\Cms\\Core\\Model\\Entity"]);
            $builder->andWhere("e.id_entity_type = :idet:", ["idet" => $type->id_entity_type]);
            $builder->columns($columns);
            foreach ($joins as $join) {
                $builder->leftJoin($join["model"],$join["conditions"],$join["alias"]);
            }
        } else {
            $builder = new Builder();
            $builder->from(["e" => static::getIndexedModel()]);
        }
        return $builder;
    }

    public static function store($idEntity = null, $data = []) {
        // Prepare entity fields
        $type = Type::findFirstByCode(static::getType());
        if (!$type) {
            throw new Type\TypeNotFoundException();
        }
        $fields = $type->getEntityField();

        if ($idEntity !== null) {
            $entity = static::findFirst(intval($idEntity));
        } else {
            $entity = new Entity([
                "id_entity_type" => $type->id_entity_type
            ]);
        }
        if (!$entity) {
            throw new EntityNotFoundException();
        }

        // Start transaction
        \Phalcon\Di::getDefault()->get("db")->begin();
        try {
            // Save basic entity
            $entity->save($data);

            // Save entity data
            foreach ($fields as $field) {
                // Check if data exists
                $value = $entity->getRelated("entity_data_".$field->backend,[
                    "conditions" => "id_entity = :ide: AND id_entity_field = :idef:",
                    "bind" => [
                        "ide" => $entity->id_entity,
                        "idef" => $field->id_entity_field
                    ]
                ])->getFirst();
                if (!$value) {
                    $valueClass = "\\Cms\\Core\\Model\\Entity\\Data\\".ucfirst($field->backend);
                    $value = new $valueClass;
                }
                if (!isset($data[$field->name]) && $value) {
                    if ($value->readAttribute("id_entity_data_".$field->backend)) {
                        $value->delete();
                    }
                } elseif (isset($data[$field->name]) && is_null($data[$field->name])) {
                    if ($value->readAttribute("id_entity_data_".$field->backend)) {
                        $value->delete();
                    }
                } elseif (isset($data[$field->name]) && !empty($data[$field->name])) {
                    $value->save([
                        "id_entity" => $entity->id_entity,
                        "id_entity_field" => $field->id_entity_field,
                        "value" => $data[$field->name]
                    ]);
                } elseif ($field->backend == "point") {
                    if (isset($data[$field->name."_lat"]) && isset($data[$field->name."_lon"])) {
                        $value->save([
                            "id_entity" => $entity->id_entity,
                            "id_entity_field" => $field->id_entity_field,
                            "value" => "POINT(".$data[$field->name."_lat"]." ".$data[$field->name."_lon"].")"
                        ]);
                    }
                }
            }

            if (\Phalcon\Di::getDefault()->get("config")->application->db->useIndexedTables) {
                \Phalcon\Di::getDefault()->get("entityIndex")->indexItem($type->code, $entity->id_entity);
            }
            // Commit transaction
            \Phalcon\Di::getDefault()->get("db")->commit();

            return true;
        } catch (EntityNotStoredException $nse) {
            // Entity not stored
            \Phalcon\Di::getDefault()->get("db")->rollback();
            return false;
        } catch (EntityNotFoundException $nfe) {
            // Entity not found
            \Phalcon\Di::getDefault()->get("db")->rollback();
            return false;
        } catch (\Exception $e) {
            // Generic Exception
            \Phalcon\Di::getDefault()->get("db")->rollback();
            return false;
        }
    }

    public static function remove($idEntity = null)
    {
        $type = Type::findFirstByCode(static::getType());
        if (!$type) {
            throw new Type\TypeNotFoundException();
        }

        if ($idEntity !== null) {
            $entity = static::findFirst(intval($idEntity));
        } else {
            $entity = new Entity([
                "id_entity_type" => $type->id_entity_type
            ]);
        }
        if (!$entity) {
            throw new EntityNotFoundException();
        }

        // Start transaction
        \Phalcon\Di::getDefault()->get("db")->begin();
        try {
            \Phalcon\Di::getDefault()->get("db")->delete("entity_data_boolean","id_entity = ".$entity->id_entity);
            \Phalcon\Di::getDefault()->get("db")->delete("entity_data_date","id_entity = ".$entity->id_entity);
            \Phalcon\Di::getDefault()->get("db")->delete("entity_data_datetime","id_entity = ".$entity->id_entity);
            \Phalcon\Di::getDefault()->get("db")->delete("entity_data_decimal","id_entity = ".$entity->id_entity);
            \Phalcon\Di::getDefault()->get("db")->delete("entity_data_integer","id_entity = ".$entity->id_entity);
            \Phalcon\Di::getDefault()->get("db")->delete("entity_data_point","id_entity = ".$entity->id_entity);
            \Phalcon\Di::getDefault()->get("db")->delete("entity_data_text","id_entity = ".$entity->id_entity);
            \Phalcon\Di::getDefault()->get("db")->delete("entity_data_varchar","id_entity = ".$entity->id_entity);
            \Phalcon\Di::getDefault()->get("db")->delete("entity","id_entity = ".$entity->id_entity);
            if (\Phalcon\Di::getDefault()->get("config")->application->db->useIndexedTables) {
                \Phalcon\Di::getDefault()->get("db")->delete($type->code."_flat","id_entity = ".$entity->id_entity);
            }

            \Phalcon\Di::getDefault()->get("db")->commit();
            return true;
        } catch (EntityNotFoundException $nfe) {
            // Entity not found
            \Phalcon\Di::getDefault()->get("db")->rollback();
            return false;
        } catch (\Exception $e) {
            // Generic Exception
            \Phalcon\Di::getDefault()->get("db")->rollback();
            return false;
        }
    }

    public function toArray($columns = null)
    {
        $data = parent::toArray($columns);
        $entity_type = $this->getEntityType();
        foreach ($entity_type->getEntityField() as $field) {
            $value = $this->getRelated("entity_data_".$field->backend,[
                "conditions" => "id_entity = :ide: AND id_entity_field = :idef:",
                "bind" => [
                    "ide" => $this->id_entity,
                    "idef" => $field->id_entity_field
                ]
            ])->getFirst();
            $data[$field->name] = $value ? $value->value : null;
        }
        return $data;
    }

}